#ifndef BCI2_2_H
#define BCI2_2_H

#include "filters.h"

void filter_bci2_2_init(void);
void filter_bci2_2_process(const float32_t *p_in_data, float32_t *p_out_data, uint32_t in_data_len);

#endif // BCI2_2_H

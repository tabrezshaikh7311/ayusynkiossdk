#ifndef HS_FILTER_H
#define HS_FILTER_H

#include <stdint.h>

void filter_hs_init(uint8_t ver);
uint32_t filter_hs_process_blockA(int16_t* pcm_in);
void filter_hs_process_blockB(int16_t *pcm_out);

#endif

#ifndef __C_FIFO_H
#define __C_FIFO_H

#include "filters.h"

typedef struct {
	int16_t wp;	        // current write pointer
	int16_t rp;	        // current read pointer
	uint32_t c_size;	    // current number of items
	uint32_t capacity;      // Capacity of queue
	float32_t * data; 		    // Pointer to array of data
	uint32_t notify_for;
} CircularBuffer_t;

#define GET_NOTIFIED 1u

uint8_t create_circular_buffer(CircularBuffer_t* fifo, float32_t * buff_ptr, uint32_t len);
uint8_t circular_buffer_is_empty(CircularBuffer_t* fifo);
uint8_t circular_buffer_is_full(CircularBuffer_t* fifo);
uint8_t add_to_circular_buffer(CircularBuffer_t* fifo, float32_t  value);
float32_t  remove_from_circular_buffer(CircularBuffer_t* fifo);
float32_t  read_from_cbuff(CircularBuffer_t* fifo, uint32_t rp);
uint32_t circular_buffer_size(CircularBuffer_t* fifo);
void free_circular_buffer(CircularBuffer_t* fifo);

#if GET_NOTIFIED
void set_size_notifier(CircularBuffer_t* fifo, void (*func)(void), uint32_t size);
#endif

#endif

#ifndef FILTER_RATE_CONVERSION_H
#define FILTER_RATE_CONVERSION_H

#include "filters.h"

#define BLOCK_SIZE_DECIMATOR			(PCM_SAMPLES_IN)
#define NO_OF_TAPS_DECIMATOR			4
#define DECIMATION_FACTOR					4

#define BLOCK_SIZE_INTERPOLATOR		(BLOCK_SIZE)
#define NO_OF_TAPS_INTERPOLATOR		4
#define UPSAMPLE_FACTOR						4


void filter_rate_conv_decimate(float32_t *p_in_data, float32_t *p_out_data, uint32_t data_len);
void filter_rate_conv_decimator_init(void);

void filter_rate_conv_interpolate(float32_t *p_in_data, float32_t *p_out_data, uint32_t data_len);
void filter_rate_conv_interpolate_init(void);

#endif // FILTER_RATE_CONVERSION_H

#ifndef FILTER_BLOCKB_H
#define FILTER_BLOCKB_H

#include "filters.h"

#define IMF_MAX_SIZE 2
#define EMD_MAX_ITERATIONS 5
#define EMD_CHOP_OFF 0.1f
#define EMD_IMF_NO 0

typedef struct{
	float32_t y;
	float32_t x;
}OUTPUT;

/** @brief Extrema types.
 */
typedef enum
{
    MINIMA,
    MAXIMA
} extrema_t;

void calculate_EMD(void);
	
#endif  // FILTER_BLOCKB_H

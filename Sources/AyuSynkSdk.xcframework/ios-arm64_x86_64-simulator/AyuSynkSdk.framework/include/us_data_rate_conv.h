#ifndef US_DATA_RATE_CONV_H
#define US_DATA_RATE_CONV_H

#ifdef __cplusplus
extern "C" {
#endif

#include "us_config.h"

void us_data_rate_conv_process(float32_t *p_in_data, float32_t *p_out_data, uint32_t data_len);
void us_data_rate_conv_init(void);

#ifdef __cplusplus
}
#endif

#endif 
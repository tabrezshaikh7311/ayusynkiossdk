#ifndef BCI2_L1_H
#define BCI2_L1_H

#include "filters.h"

void filter_bci2_ls1_init(void);
void filter_bci2_ls1_process(const float32_t *p_in_data, float32_t *p_out_data, uint32_t in_data_len);

void filter_bci2_ls2_init(void);
void filter_bci2_ls2_process(const float32_t* p_in_data, float32_t* p_out_data, uint32_t in_data_len);

#endif // BCI2_L1_H

#ifndef NO_FILTER_H
#define NO_FILTER_H

#include <stdint.h>

void filter_no_init(void);
uint32_t filter_no_process_blockA(int16_t *pcm_in);
void filter_no_process_blockB(int16_t *pcm_out);

#endif

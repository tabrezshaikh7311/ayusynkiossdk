#ifndef FIR_L1_L2_H
#define FIR_L1_L2_H

#include "filters.h"

void filter_fir_ls1_init(void);
void filter_fir_ls1_process(const float32_t *p_in_data, float32_t *p_out_data, uint32_t in_data_len);
//void filter_fir_ls2_init(void);
//void filter_fir_ls2_process(const float32_t* p_in_data, float32_t* p_out_data, uint32_t in_data_len);

#endif // FIR_L1_L2_H

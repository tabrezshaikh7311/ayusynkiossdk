#ifndef FILTER_UTILS_H
#define FILTER_UTILS_H

#include "filters.h"

#define 	SAMPLE_MAX							(0.999969482421875f)
#define 	SAMPLE_MIN							(-1.0f)

void gamma_one(float32_t *p_src, uint32_t size);
void gamma_two(float32_t *p_src, uint32_t size);
void gamma_three(float32_t* p_src, uint32_t size, uint8_t iter);
void gamma_four(float32_t* p_src, uint32_t size, float32_t gamma_4_a);
void range_check_float32(float32_t* psrc, uint32_t len);
void multiply_by_n(float32_t* psrc, float32_t n, uint32_t size);
void Filter_Error_Handler(void);

#endif  // FILTER_UTILS_H

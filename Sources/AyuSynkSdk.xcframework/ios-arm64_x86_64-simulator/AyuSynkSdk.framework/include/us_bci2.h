#ifndef US_BCI2_H
#define US_BCI2_H

#ifdef __cplusplus
extern "C" {
#endif

#include "us_config.h"

void us_bci2_init(void);
void us_bci2_process(const float32_t *p_in_data, float32_t *p_out_data, uint32_t in_data_len);

#ifdef __cplusplus
}
#endif

#endif
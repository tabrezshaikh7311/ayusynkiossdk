//
//  ViewController.swift
//  AyuSynk_IOS_SDK
//
//  Created by Tabrez Chowkar on 03/07/23.
//

import UIKit
import AyuSynkSdk
import CorePlot

class MainViewController: UIViewController {


    @IBOutlet weak var connectBtn: UIButton!
    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var generateReportBtn: UIButton!
    @IBOutlet weak var shareReportBtn: UIButton!
    @IBOutlet weak var filters: UISegmentedControl!
    @IBOutlet weak var graph: CPTGraphHostingView!
    @IBOutlet weak var connectionLabel: UILabel!
    @IBOutlet weak var strenghtLabel: UILabel!
    @IBOutlet weak var batteryLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBAction func connectButtonClicked(_ sender: Any) {
        if(AyuSynk.isDeviceConnected() == DeviceConnectionState.DEVICE_DISCONNECTED){
            let vc = UIStoryboard.init(name: "Main", bundle:Bundle.main).instantiateViewController(withIdentifier: "connect") as? ConnectViewController
            vc!.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc!, animated:true)
        }else{
            AyuSynk.disconnect()
        }
    }
    
    @IBAction func recordButtonClicked(_ sender: Any) {
        AyuSynk.startRecording()
        setUIForRecording()
    }
    
    @IBAction func pauseButtonClicked(_ sender: Any) {
        if(isPlayingRecordedSound){
            AyuSynk.stopAudioPlayback()
            isPlayingRecordedSound = false
        }else{
            AyuSynk.pauseRecording()
            isRecordingPaused = true
        }
        
        if(lastRecordedData == nil){
            recordBtn.isEnabled = true
            pauseBtn.isEnabled = false
        }else{
            setUIForPlayingComplete()
        }
    }
    
    @IBAction func playButtonClicked(_ sender: Any) {
        playLastRecordedData()
    }
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        //you can rename file name based on position from where it is recorded and append with timestamp to avoid duplicate
        if let fileURL = generateFileForRecordedAudio(){
            let activityViewController = UIActivityViewController(activityItems: [fileURL], applicationActivities: nil)
            
            // On iPad, you need to provide a source view and rect to specify the anchor point for the popover.
            if let popoverPresentationController = activityViewController.popoverPresentationController {
                popoverPresentationController.sourceView = view
                popoverPresentationController.sourceRect = CGRect(x: 0, y: 0, width: 50, height: 50)
            }
            
            present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func generateReportButtonClicked(_ sender: Any) {
        if let fileURL = generateFileForRecordedAudio(){
            showSpinner()
            generateReportBtn.isEnabled = false
            shareReportBtn.isEnabled = false
            let soundData = HeartSoundData(fileUrl: fileURL, locationName: LocationType.Heart.aortic)
            let soundFile = SoundFile(soundData: soundData)
            AyuSynk.generateDiagnosisReport(soundFile: soundFile)
        }
    }
    
    @IBAction func shareReportButtonClicked(_ sender: Any) {
        var link = ""
        if(soundFileReport != nil){
            if let reports : [ReportData] = soundFileReport!.getReports() {
                for reportData in reports {
                    link.append(reportData.position!);
                    link.append(":\n");
                    link.append(reportData.reportURL!);
                    link.append("\n");
                    if(reportData.soundType == .HEART){
                        link.append("Abnormality Detected:");
                        link.append("\n");
                        link.append(reportData.heartConditionDetected!);
                        link.append("(Confidence:");
                        link.append(reportData.heartConditionConfidence!);
                        link.append(")\n");
                    }
                }
                let activityViewController = UIActivityViewController(activityItems: [link], applicationActivities: nil)
                
                // On iPad, you need to provide a source view and rect to specify the anchor point for the popover.
                if let popoverPresentationController = activityViewController.popoverPresentationController {
                    popoverPresentationController.sourceView = view
                    popoverPresentationController.sourceRect = CGRect(x: 0, y: 0, width: 50, height: 50)
                }
                
                present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func filterSelected(_ sender: Any) {
        let filter = filters.selectedSegmentIndex
        if(filter == 0){
            AyuSynk.setFilter(filter: FilterType.NoFilter)//No Filter selected
        }else if(filter == 1){
            AyuSynk.setFilter(filter: FilterType.HeartFilter)//Heart Filter selected
        }else if(filter == 2){
            AyuSynk.setFilter(filter: FilterType.LungFilter)//Lung Filter selected
        }
    }

    
    private var lastRecordedData : Data?
    private var recordID = ""
    private var isRecordingPaused = false
    private var isPlayingRecordedSound = false
    private var soundFileReport: SoundFile?
    
    private func showSpinner() {
        activityIndicator.startAnimating()
        loadingView.isHidden = false
    }

    private func hideSpinner() {
        activityIndicator.stopAnimating()
        loadingView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "AyuSynkSdk"
        print("AyuSynkSdk Ver. \(AyuSynk.getVersion())")
        AyuSynk.verify(clientId: "YOUR_CLIENT_ID", verificationListener: self)
        hideSpinner()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let myColor = UIColor(red: 0.5, green: 0.7, blue: 0.9, alpha: 1.0)
        AyuSynk.setupVisualizerView(view: graph, color: myColor.cgColor)
        AyuSynk.setAyuDeviceListener(listener: self)
        AyuSynk.setRecorderListener(listener: self)
        AyuSynk.setDiagnosisReportUpdateListener(listener: self)
        setViewBasedOnDeviceConnectionState(isConnected: AyuSynk.isDeviceConnected() == DeviceConnectionState.DEVICE_CONNECTED)
    }
    
    private func setViewBasedOnDeviceConnectionState(isConnected: Bool){
        if(isConnected){
            strenghtLabel.isHidden = false
            batteryLabel.isHidden = false
            setDeviceStrength(strength: AyuSynk.getDeviceStrength())
            deviceBatteryUpdate(battery: AyuSynk.getCurrentBatteryLevel());
            connectionLabel.text = "Device Connected"
            connectBtn.setTitle("Disconnect", for: .normal)
        }else{
            connectionLabel.text = "Device Disconnected"
            connectBtn.setTitle("Connect", for: .normal)
            strenghtLabel.isHidden = true
            batteryLabel.isHidden = true
        }
        recordBtn.isEnabled = isConnected
        filters.isEnabled = isConnected
    }
    
    private func setUIForRecording(){
        recordBtn.isEnabled = false
        saveBtn.isEnabled = false
        playBtn.isEnabled = false
        generateReportBtn.isEnabled = false
        pauseBtn.isEnabled = true
    }
    
    private func setUIForPlaying(){
        recordBtn.isEnabled = false
        pauseBtn.isEnabled = true
    }
    
    private func setUIForPlayingComplete(){
        isPlayingRecordedSound = false
        recordBtn.isEnabled = AyuSynk.isDeviceConnected() == DeviceConnectionState.DEVICE_CONNECTED
        pauseBtn.isEnabled = false
        saveBtn.isEnabled = true
        generateReportBtn.isEnabled = true
        playBtn.isEnabled = true
    }
    
    private func playLastRecordedData(){
        if(!recordID.isEmpty){
            isPlayingRecordedSound = true
            setUIForPlaying()
            AyuSynk.playAudio(recordID: recordID)
        }else{
         print("No Recording available.Record first")
        }
    }
    
    private func generateFileForRecordedAudio() -> URL? {
        return AyuSynk.generateFile(fileName: "soundFileName", sampleData: lastRecordedData!)
    }
    
    deinit{
        AyuSynk.close()
    }
}

extension MainViewController : VerificationListener {
    func verificationResult(isVerified: Bool, error: String) {
        print("isverified",isVerified)
        if(!isVerified){
            print("error",error)
        }
    }
}

extension MainViewController : AyuDeviceListener {
    func deviceConnectionStrength(strength: AyuSynkSdk.DeviceStrength) {
        setDeviceStrength(strength: strength)
    }
    
    func setDeviceStrength(strength: AyuSynkSdk.DeviceStrength){
        if(strength == DeviceStrength.DEVICE_SIGNAL_WEAK){
            strenghtLabel.text = "Connection: Weak";
        }else{
            strenghtLabel.text = "Connection: Strong";
        }
    }
    
    func deviceConnectionState(state: AyuSynkSdk.DeviceConnectionState) {
        setViewBasedOnDeviceConnectionState(isConnected: state == DeviceConnectionState.DEVICE_CONNECTED)
    }
    
    func deviceBattery(battery: Int) {
        deviceBatteryUpdate(battery: battery)
    }
    
    func deviceBatteryUpdate(battery: Int){
        batteryLabel.text = "Battery: \(battery)%"
    }
    
    func deviceConnectionFailed(error: String) {
        
    }
}

extension MainViewController : RecorderListener {
    
    func elapsedTime(min: Int, secs: Int) {
        let secsFloat = Float(secs)/Float(10)
        self.progressView.progress = secsFloat
    }
    
    func recordingComplete(recordID: String) {
        self.recordID = recordID
        lastRecordedData = AyuSynk.getAudioData(recordID: recordID)
        setUIForPlayingComplete()
    }
    
    func playingComplete() {
        setUIForPlayingComplete()
    }
    
}

extension MainViewController : DiagnosisReportUpdateListener {
    
    func reportRequestAdded(soundFile: SoundFile) {
        print("Report request added")
    }
    
    func reportGenerated(soundFile: AyuSynkSdk.SoundFile) {
        print("Report generated")
        hideSpinner()
        generateReportBtn.isEnabled = true
        shareReportBtn.isEnabled = true
        soundFileReport = soundFile
    }
    
    func onReportGenerationError(error: String?) {
        hideSpinner()
        print("Report generation error")
    }
    
    
}


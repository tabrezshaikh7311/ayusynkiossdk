//
//  ConnectFragment.swift
//  AyuSynk_IOS_SDK
//
//  Created by Ayu Devices on 27/10/23.
//

import UIKit
import Foundation
import AyuSynkSdk

class ConnectViewController : UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDevicesUI: UIView!
    @IBOutlet weak var loadingView: UIView! {
      didSet {
        loadingView.layer.cornerRadius = 6
      }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var device:AyuSynkSdk.Device?
    var isConnectedToDevice = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Connect Device"
        AyuSynk.setDeviceScanListener(listener: self)
        AyuSynk.setAyuDeviceListener(listener: self)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func showSpinner() {
        activityIndicator.startAnimating()
        loadingView.isHidden = false
    }

    private func hideSpinner() {
        activityIndicator.stopAnimating()
        loadingView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.searchForDevice()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AyuSynk.stopScan()
    }
    
    @IBAction func tapRescanButtonAction(_ sender: Any) {
        self.searchForDevice()
    }
    

    func searchForDevice(){
        DispatchQueue.main.async {
            self.showSpinner()
            self.tableView.reloadData()
            self.noDevicesUI.isHidden = true
        }
        
        print("Searching has been restarted")
        AyuSynk.startScan()
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            self.triggerConnectionTimeOut()
        }
    }
    
    func triggerConnectionTimeOut(){
        print("Connection time out triggered")
        self.hideSpinner()
        if self.device == nil {
            noDevicesUI.isHidden = false
        }
    }
}

extension ConnectViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//            self.triggerConnectionTimeOut()
        return self.device == nil ? 0 : 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ASConnectionTypeCell") as! ASConnectionTypeCell
        cell.selectionStyle = .none
        cell.configure(device: self.device!)
        return cell
    }
    
    func connect(toDevice:AyuSynkSdk.Device){
        self.showSpinner()
        AyuSynk.connect(deviceUUID: toDevice.uniqueIdentifier)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            if self.isConnectedToDevice == false{
                self.searchForDevice()
            }
        }
    }
}

extension ConnectViewController : DeviceScanListener,AyuDeviceListener {
    func deviceConnectionStrength(strength: AyuSynkSdk.DeviceStrength) {
        
    }
    
    func deviceConnectionState(state: AyuSynkSdk.DeviceConnectionState) {
        if(state == DeviceConnectionState.DEVICE_CONNECTED){
            DispatchQueue.main.async {
                self.isConnectedToDevice = true
                self.hideSpinner()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func deviceBattery(battery: Int) {
        
    }
    
    func deviceConnectionFailed(error: String) {
        
    }
    
    func onScanStart() {
        
    }
    
    func onDeviceFound(device: AyuSynkSdk.Device) {
        self.device = device
        DispatchQueue.main.async {
            print("Node discovered")
            self.triggerConnectionTimeOut()
            self.tableView.reloadData()
        }
    }
    
    func onScanFinish() {
        
    }
    
    func onScanFailed(error: String) {
        
    }
    
    
}


class ASConnectionTypeCell:UITableViewCell{
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var deviceNameLabel: UILabel!
    var device:AyuSynkSdk.Device? = nil
    
    func configure(device:AyuSynkSdk.Device){
        self.device = device
        self.deviceNameLabel.text = "AyuSynk"
        self.instructionLabel.text = device.uniqueIdentifier
    }
    
    @IBAction func connectButtonTapAction(_ sender: Any) {
        if let vc = self.parentViewController as? ConnectViewController,let n = self.device{
            vc.connect(toDevice: n)
        }
    }
    
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder?.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

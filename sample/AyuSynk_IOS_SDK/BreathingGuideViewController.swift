//
//  BreathingGuideViewController.swift
//  AyuSynk_IOS_SDK
//
//  Created by Ayu Devices on 08/11/24.
//

import Foundation
import UIKit
import AyuSynkSdk

/*
 This class demonstrate use of BreathingGuide
 Breathing guide was made available in Sdk Ver. 1.2.0
 */
class BreathingGuideViewController: UIViewController {
    
    var breathingGuide: BreathingGuideView!
    
    @IBAction func onStartClicked(_ sender: Any) {
        breathingGuide.startBreathingAnimation()
    }
    
    @IBAction func onStopClicked(_ sender: Any) {
        breathingGuide.stopBreathingAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        breathingGuide = BreathingGuideView(frame: CGRect(x: 50, y: 50, width: 200, height: 200))
        breathingGuide.setFontSize(18)
        breathingGuide.setTextColor(.black)
        breathingGuide.setOuterBorderColor(.systemBlue)
        breathingGuide.setInnerCircleColor(.systemBlue)
        view.addSubview(breathingGuide)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        breathingGuide.stopBreathingAnimation()
    }
}
